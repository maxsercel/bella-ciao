import React from "react";
import {
    FaFacebookF,
    FaInstagram,
    FaTwitter,
    FaLinkedin,
} from "react-icons/fa";

import ContentWrapper from "../contentWrapper/ContentWrapper";

import "./style.scss";

const Footer = () => {
    return (
        <footer className="footer">
            <ContentWrapper>
                <ul className="menuItems">
                    <li className="menuItem">Terms Of Use</li>
                    <li className="menuItem">Telegram</li>
                </ul>
                <div className="infoText">
                This site does not store any files on its server. All contents are provided by non-affiliated third parties.
                </div>
                
            </ContentWrapper>
        </footer>
    );
};

export default Footer;
