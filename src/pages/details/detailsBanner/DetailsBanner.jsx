import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";
import dayjs from "dayjs";

import "./style.scss";

import ContentWrapper from "../../../components/contentWrapper/ContentWrapper";
import useFetch from "../../../hooks/useFetch";
import Genres from "../../../components/genres/Genres";
import CircleRating from "../../../components/circleRating/CircleRating";
import Img from "../../../components/lazyLoadImage/Img.jsx";
import PosterFallback from "../../../assets/no-poster.png";
import { PlayIcon } from "../Playbtn";
import VideoPopup from "../../../components/videoPopup/VideoPopup";

const DetailsBanner = ({ video, crew }) => {
    const [show, setShow] = useState(false);
    const [videoId, setVideoId] = useState(null);

    const [selectedServer, setSelectedServer] = useState(1);
    const [selectedSeason, setSelectedSeason] = useState(1);
    const [selectedEpisode, setSelectedEpisode] = useState(1);

    const { mediaType, id } = useParams();
    const { data, loading } = useFetch(`/${mediaType}/${id}`);

    const { url } = useSelector((state) => state.home);

    const _genres = data?.genres?.map((g) => g.id);

    const director = crew?.filter((f) => f.job === "Director");
    const writer = crew?.filter(
        (f) => f.job === "Screenplay" || f.job === "Story" || f.job === "Writer"
    );

    const toHoursAndMinutes = (totalMinutes) => {
        const hours = Math.floor(totalMinutes / 60);
        const minutes = totalMinutes % 60;
        return `${hours}h${minutes > 0 ? ` ${minutes}m` : ""}`;
    };

    const getIframeSrc = () => {
        const baseUrls = {
            1: {
                movie: `https://vidsrc.to/embed/movie/${id}`,
                tv: `https://vidsrc.to/embed/tv/${id}/${selectedSeason}/${selectedEpisode}`,
            },
            2: {
                movie: `https://moviesapi.club/movie/${id}`,
                tv: `https://moviesapi.club/tv/${id}-${selectedSeason}-${selectedEpisode}`,
            },
            3: {
                movie: `https://multiembed.mov/?video_id=${id}&tmdb=1`,
                tv: `https://multiembed.mov/?video_id=${id}&tmdb=1&s=${selectedSeason}&e=${selectedEpisode}`,
            },
        };
        return mediaType === "movie" ? baseUrls[selectedServer].movie : baseUrls[selectedServer].tv;
    };

    const getSeasonsData = () => {
        if (data?.name === "Money Heist") {
            return [
                { season_number: 1, episodes: Array.from({ length: 13 }, (_, i) => ({ episode_number: i + 1 })) },
                { season_number: 2, episodes: Array.from({ length: 9 }, (_, i) => ({ episode_number: i + 1 })) },
                { season_number: 3, episodes: Array.from({ length: 8 }, (_, i) => ({ episode_number: i + 1 })) },
                { season_number: 4, episodes: Array.from({ length: 8 }, (_, i) => ({ episode_number: i + 1 })) },
                { season_number: 5, episodes: Array.from({ length: 10 }, (_, i) => ({ episode_number: i + 1 })) },
            ];
        }
        return data?.seasons || [];
    };

    const seasonsData = getSeasonsData();

    return (
        <div className="detailsBanner">
            {!loading ? (
                <>
                    {!!data && (
                        <React.Fragment>
                            <div className="backdrop-img">
                                <Img src={url.backdrop + data.backdrop_path} />
                            </div>
                            <div className="opacity-layer"></div>
                            <ContentWrapper>
                                <div className="content">
                                    <div className="left">
                                        {data.poster_path ? (
                                            <Img
                                                className="posterImg"
                                                src={
                                                    url.backdrop +
                                                    data.poster_path
                                                }
                                            />
                                        ) : (
                                            <Img
                                                className="posterImg"
                                                src={PosterFallback}
                                            />
                                        )}
                                    </div>
                                    <div className="right">
                                        <div className="title">
                                            {`${
                                                data.name || data.title
                                            } (${dayjs(
                                                data?.release_date
                                            ).format("YYYY")})`}
                                        </div>
                                        <div className="subtitle">
                                            {data.tagline}
                                        </div>

                                        <Genres data={_genres} />

                                        <div className="row">
                                            <CircleRating
                                                rating={data.vote_average.toFixed(
                                                    1
                                                )}
                                            />
                                            <div
                                                className="playbtn"
                                                onClick={() => {
                                                    setShow(true);
                                                    setVideoId(video.key);
                                                }}
                                            >
                                                
                                            </div>
                                        </div>

                                        <div className="overview">
                                            <div className="heading">
                                                Overview
                                            </div>
                                            <div className="description">
                                                {data.overview}
                                            </div>
                                        </div>

                                        <div className="info">
                                            {data.status && (
                                                <div className="infoItem">
                                                    <span className="text bold">
                                                        Status:{" "}
                                                    </span>
                                                    <span className="text">
                                                        {data.status}
                                                    </span>
                                                </div>
                                            )}
                                            {data.release_date && (
                                                <div className="infoItem">
                                                    <span className="text bold">
                                                        Release Date:{" "}
                                                    </span>
                                                    <span className="text">
                                                        {dayjs(
                                                            data.release_date
                                                        ).format("MMM D, YYYY")}
                                                    </span>
                                                </div>
                                            )}
                                            {data.runtime && (
                                                <div className="infoItem">
                                                    <span className="text bold">
                                                        Runtime:{" "}
                                                    </span>
                                                    <span className="text">
                                                        {toHoursAndMinutes(
                                                            data.runtime
                                                        )}
                                                    </span>
                                                </div>
                                            )}
                                        </div>

                                        {director?.length > 0 && (
                                            <div className="info">
                                                <span className="text bold">
                                                    Director:{" "}
                                                </span>
                                                <span className="text">
                                                    {director?.map((d, i) => (
                                                        <span key={i}>
                                                            {d.name}
                                                            {director.length -
                                                                1 !==
                                                                i && ", "}
                                                        </span>
                                                    ))}
                                                </span>
                                            </div>
                                        )}

                                        {writer?.length > 0 && (
                                            <div className="info">
                                                <span className="text bold">
                                                    Writer:{" "}
                                                </span>
                                                <span className="text">
                                                    {writer?.map((d, i) => (
                                                        <span key={i}>
                                                            {d.name}
                                                            {writer.length -
                                                                1 !==
                                                                i && ", "}
                                                        </span>
                                                    ))}
                                                </span>
                                            </div>
                                        )}

                                        {data?.created_by?.length > 0 && (
                                            <div className="info">
                                                <span className="text bold">
                                                    Creator:{" "}
                                                </span>
                                                <span className="text">
                                                    {data?.created_by?.map(
                                                        (d, i) => (
                                                            <span key={i}>
                                                                {d.name}
                                                                {data
                                                                    ?.created_by
                                                                    .length -
                                                                    1 !==
                                                                    i && ", "}
                                                            </span>
                                                        )
                                                    )}
                                                </span>
                                            </div>
                                        )}

                                        {/* Player and Server Selection */}
                                        <div className="player-container">
                                            <iframe
                                                src={getIframeSrc()}
                                                className="video-player"
                                                frameBorder="0"
                                                allowFullScreen
                                            ></iframe>
                                            <div className="server-buttons">
                                                <button className={selectedServer === 1 ? 'active' : ''} onClick={() => setSelectedServer(1)}>Server 1</button>
                                                <button className={selectedServer === 2 ? 'active' : ''} onClick={() => setSelectedServer(2)}>Server 2</button>
                                                <button className={selectedServer === 3 ? 'active' : ''} onClick={() => setSelectedServer(3)}>Server 3</button>
                                            </div>
                                            {mediaType === "tv" && seasonsData.length > 0 && (
                                                <div className="season-episode-selector">
                                                    <select className="dropdown" onChange={(e) => setSelectedSeason(e.target.value)}>
                                                        {seasonsData?.map((season) => (
                                                            <option key={season.season_number} value={season.season_number}>
                                                                Season {season.season_number}
                                                            </option>
                                                        ))}
                                                    </select>
                                                    <select className="dropdown" onChange={(e) => setSelectedEpisode(e.target.value)}>
                                                        {seasonsData[selectedSeason - 1]?.episodes?.map((episode) => (
                                                            <option key={episode.episode_number} value={episode.episode_number}>
                                                                Episode {episode.episode_number}
                                                            </option>
                                                        ))}
                                                    </select>
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                <VideoPopup
                                    show={show}
                                    setShow={setShow}
                                    videoId={videoId}
                                    setVideoId={setVideoId}
                                />
                            </ContentWrapper>
                        </React.Fragment>
                    )}
                </>
            ) : (
                <div className="detailsBannerSkeleton">
                    <ContentWrapper>
                        <div className="left skeleton"></div>
                        <div className="right">
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                            <div className="row skeleton"></div>
                        </div>
                    </ContentWrapper>
                </div>
            )}
        </div>
    );
};

export default DetailsBanner;
