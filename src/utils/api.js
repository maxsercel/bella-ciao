import axios from "axios";

const BASE_URL = "https://api.themoviedb.org/3";
const TMDB_TOKEN = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkNTZhY2E1YzAxMGUyYjYxMWJlYmQ4OGE0N2NlZTJmZCIsIm5iZiI6MTcxOTc4NjAyMy44MzA5NzcsInN1YiI6IjY2NzdhYTc0YTJhZjI4M2NlZGYyNGJiYyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.WeoMKhGR_kuoiqk7mVl5h9BYeMg976y0ikepGPg4ah4";

const headers = {
    Authorization: "bearer " + TMDB_TOKEN,
};

export const fetchDataFromApi = async (url, params) => {
    try {
        const { data } = await axios.get(BASE_URL + url, {
            headers,
            params,
        });
        return data;
    } catch (err) {
        console.log(err);
        return err;
    }
};
